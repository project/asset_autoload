<?php

namespace Drupal\Tests\asset_autoload\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the Asset Autoload module.
 *
 * @package Drupal\Tests\asset_autoload\Functional
 * @group asset_autoload
 */
class AssetAutoloadTest extends BrowserTestBase {
  /**
   * @inheritdoc
   */
  protected static $modules = ['node', 'asset_autoload'];

  /**
   * @inheritdoc
   */
  protected $defaultTheme = 'asset_autoload_test_theme';

  /**
   * @inheritdoc
   */
  protected function setUp(): void {
    parent::setUp();

    // Create node type.
    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'page',
        'name' => 'Basic page',
        'display_submitted' => FALSE,
      ]);
    $type->save();
    $this->container->get('router.builder')->rebuild();

    // Create a node.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Asset autoload test',
    ]);
    $node->save();
  }

  /**
   * Test /node/1.
   */
  public function testThemeNodePage() {
    $this->drupalGet('/node/1');

    // Test html.
    $this->assertSession()->elementExists('css', 'body.html.html--node.html--node--1');

    // Test page.
    $this->assertSession()->elementExists('css', 'div.page.page--node.page--node--1');
    $this->assertSession()->elementExists('css', 'link[href^="/modules/contrib/asset_autoload/tests/themes/asset_autoload_test_theme/templates/page/page.css"]');
    $this->assertSession()->elementExists('css', 'script[src^="/modules/contrib/asset_autoload/tests/themes/asset_autoload_test_theme/templates/page/page.js"]');

    // Test node.
    $this->assertSession()->elementExists('css', 'article.node.node--full.node--page.node--page--full.node--1.node--1--full');
    $this->assertSession()->elementExists('css', 'link[href^="/modules/contrib/asset_autoload/tests/themes/asset_autoload_test_theme/templates/node/node--page/node--page.css"]');
    $this->assertSession()->elementExists('css', 'script[src^="/modules/contrib/asset_autoload/tests/themes/asset_autoload_test_theme/templates/node/node--page/node--page.js"]');

    // Test globally attached assets.
    $this->assertSession()->elementExists('css', 'link[href^="/modules/contrib/asset_autoload/tests/themes/asset_autoload_test_theme/templates/test-attached.css"]');
    $this->assertSession()->elementExists('css', 'script[src^="/modules/contrib/asset_autoload/tests/themes/asset_autoload_test_theme/templates/test-attached.js"]');

    // Test unattached assets.
    $this->assertSession()->elementNotExists('css', 'link[href^="/modules/contrib/asset_autoload/tests/themes/asset_autoload_test_theme/templates/test-unattached.css"]');
    $this->assertSession()->elementNotExists('css', 'script[src^="/modules/contrib/asset_autoload/tests/themes/asset_autoload_test_theme/templates/test-unattached.js"]');
  }

}
