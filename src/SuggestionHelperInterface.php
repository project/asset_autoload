<?php

namespace Drupal\asset_autoload;

/**
 * Provides an interface for suggestion helpers.
 *
 * @package Drupal\asset_autoload
 */
interface SuggestionHelperInterface {

  /**
   * Return a list theme suggestions based on the given hook_preprocess()
   * arguments.
   * 
   * @param array $variables 
   * @param string $hook 
   * @param array $info 
   *
   * @return array
   */
  public function getSuggestions(array $variables, string $hook, array $info): array;

}
