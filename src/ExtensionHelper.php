<?php

namespace Drupal\asset_autoload;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeExtensionList;

/**
 * Implements the extension helper interface.
 *
 * @package Drupal\asset_autoload
 */
class ExtensionHelper implements ExtensionHelperInterface {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected $themeExtensionList;

  /**
   * Constructs a new extension helper object.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   * @param \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList
   * @param \Drupal\Core\Theme\ThemeManager $themeManager
   */
  public function __construct(
    ModuleExtensionList $moduleExtensionList,
    ThemeExtensionList $themeExtensionList,
  ) {
    $this->moduleExtensionList = $moduleExtensionList;
    $this->themeExtensionList = $themeExtensionList;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllInstalledInfo(): array {
    $module_infos = $this->moduleExtensionList->getAllInstalledInfo();
    $theme_infos = $this->themeExtensionList->getAllInstalledInfo();
    return array_merge($module_infos, $theme_infos);
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionInfo(string $extension): array {
    if ($this->moduleExtensionList->exists($extension)) {
      return $this->moduleExtensionList->getExtensionInfo($extension);
    }
    if ($this->themeExtensionList->exists($extension)) {
      return $this->themeExtensionList->getExtensionInfo($extension);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPath(string $extension): string {
    if ($this->moduleExtensionList->exists($extension)) {
      return $this->moduleExtensionList->getPath($extension);
    }
    if ($this->themeExtensionList->exists($extension)) {
      return $this->themeExtensionList->getPath($extension);
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(string $extension): array {
    if ($this->moduleExtensionList->exists($extension)) {
      return $this->moduleExtensionList->get($extension)->requires;
    }
    if ($this->themeExtensionList->exists($extension)) {
      return $this->themeExtensionList->get($extension)->requires;
    }
    return [];
  }

}
