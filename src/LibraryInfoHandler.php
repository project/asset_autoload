<?php

namespace Drupal\asset_autoload;

/**
 * Implements the library info handler interface.
 *
 * @package Drupal\asset_autoload
 */
class LibraryInfoHandler implements LibraryInfoHandlerInterface {

  /**
   * The scanner.
   *
   * @var \Drupal\asset_autoload\Scanner
   */
  protected $scanner;

  /**
   * Constructs a new library info handler object.
   *
   * @param Scanner $scanner
   */
  public function __construct(
    Scanner $scanner,
  ) {
    $this->scanner = $scanner;
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoAlter(array &$libraries, string $extension): void {
    $scanned_libraries = $this->scanner->getLibraries($extension);
    $libraries = array_replace_recursive($scanned_libraries, $libraries);
  }

}
