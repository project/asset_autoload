<?php

namespace Drupal\asset_autoload;

use Drupal\Core\Theme\ThemeManager;

/**
 * Implements the preprocess handler interface.
 *
 * @package Drupal\asset_autoload
 */
class PreprocessHandler implements PreprocessHandlerInterface {

  /**
   * The suggestion helper.
   *
   * @var \Drupal\asset_autoload\SuggestionHelper
   */
  protected $suggestionHelper;

  /**
   * The scanner.
   *
   * @var \Drupal\asset_autoload\Scanner
   */
  protected $scanner;

  /**
   * The theme manager.
   * 
   * @var \Drupal\Core\Theme\ThemeManager
   */
  protected $themeManager;


  /**
   * Constructs a new preprocess handler object.
   *
   * @param \Drupal\asset_autoload\SuggestionHelper $suggestionHelper
   */
  public function __construct(
    SuggestionHelper $suggestionHelper,
    Scanner $scanner,
    ThemeManager $themeManager,
  ) {
    $this->suggestionHelper = $suggestionHelper;
    $this->scanner = $scanner;
    $this->themeManager = $themeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, string $hook, array $info): void {
    $suggestions = $this->suggestionHelper->getSuggestions($variables, $hook, $info);
    $active_themes = $this->getActiveThemes();

    foreach ($suggestions as $suggestion) {
      $extensions = $this->scanner->getExtensions($suggestion);
      foreach ($extensions as $extension => $extension_info) {
        if ($extension_info['type'] === 'theme') {
          if (!in_array($extension, $active_themes)) {
            continue;
          }
        }
        $variables['#attached']['library'][] = "$extension/$suggestion";
      }
    }

    $variables['suggestions'] = $suggestions;
  }

  /**
   * Returns the names of the currently active theme and all its base themes.
   *
   * @return array
   */
  protected function getActiveThemes(): array {
    static $active_themes;

    if (is_array($active_themes)) {
      return $active_themes;
    }

    $active_theme = $this->themeManager->getActiveTheme();
    $base_themes = $active_theme->getBaseThemeExtensions();
    $active_themes = [$active_theme->getName(), ...array_keys($base_themes)];
    return $active_themes;
  }

}
