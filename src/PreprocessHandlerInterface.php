<?php

namespace Drupal\asset_autoload;

/**
 * Provides an interface for preprocess handlers.
 * 
 * @package Drupal\asset_autoload
 */
interface PreprocessHandlerInterface {

  /**
   * Implements hook_preprocess().
   */
  public function preprocess(array &$variables, string $hook, array $info): void;

}
