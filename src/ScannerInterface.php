<?php

namespace Drupal\asset_autoload;

/**
 * Provides an interface for scanners.
 *
 * @package Drupal\asset_autoload
 */
interface ScannerInterface {

  /**
   * Returns the libraries in the specified extension.
   */
  public function getLibraries(string $extension): array;

  /**
   * Returns the extensions providing the specified library.
   */
  public function getExtensions(string $library): array;

}
