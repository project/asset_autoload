<?php

namespace Drupal\asset_autoload;

/**
 * Provides an interface for library info handlers.
 *
 * @package Drupal\asset_autoload
 */
interface LibraryInfoHandlerInterface {

  /**
   * Implements hook_library_info_alter().
   */
  public function libraryInfoAlter(array &$libraries, string $extension): void;

}
