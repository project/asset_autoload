<?php

namespace Drupal\asset_autoload;

use Drupal\Core\File\FileSystem;

/**
 * Implements the file system hepler interface.
 *
 * @package Drupal\asset_autoload
 */
class FileSystemHelper implements FileSystemHelperInterface {

  /**
   * The file system service.
   *
   * @var FileSystem
   */
  protected $fileSystem;

  /**
   * Constructs a new file system helper object.
   *
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *
   * @return void 
   */
  public function __construct(
    FileSystem $fileSystem,
  ) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public function scanDirectory(string $dir, string $mask, array $options = []): array {
    $default_options = [
      'nomask' => '/^(node_modules|bower_components|vendor)$/',
    ];
    $merged_options = array_merge($default_options, $options);
    return $this->fileSystem->scanDirectory($dir, $mask, $merged_options);
  }

}
