<?php

namespace Drupal\asset_autoload;

/**
 * Provides an interface for extension helpers.
 *
 * @package Drupal\asset_autoload
 */
interface ExtensionHelperInterface {

  /**
   * Returns an array of info files information of available extensions.
   *
   * @param string $extension
   *
   * @return array
   */
  public function getAllInstalledInfo(): array;

  /**
   * Returns information about a specified extension.
   *
   * @param string $extension
   *
   * @return array
   */
  public function getExtensionInfo(string $extension): array;

  /**
   * Gets the path to an extension.
   *
   * @param string $extension
   *
   * @return string
   */
  public function getPath(string $extension): string;

  /**
   * Finds all dependencies for the specified extension.
   * 
   * @param string $extension
   * 
   * @return array
   */
  public function getDependencies(string $extension): array;

}
