<?php

namespace Drupal\asset_autoload;

/**
 * Provides an interface for file system helpers.
 *
 * @package Drupal\asset_autoload
 */
interface FileSystemHelperInterface {

  /**
   * Finds all files that match a given mask in a given directory.
   *
   * @see \Drupal\Core\File\FileSystem
   */
  public function scanDirectory(string $dir, string $mask, array $options = []): array;

}
