<?php

namespace Drupal\asset_autoload;

use Drupal\Component\Utility\Html;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Theme\ThemeManager;

/**
 * Implements the suggestion helper interface.
 *
 * @package Drupal\asset_autoload
 */
class SuggestionHelper implements SuggestionHelperInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManager
   */
  protected $themeManager;

  /**
   * Constructs a new suggestion helper object.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   * @param \Drupal\Core\Theme\ThemeManager $themeManager
   */
  public function __construct(
    ModuleHandler $moduleHandler,
    ThemeManager $themeManager,
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->themeManager = $themeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getSuggestions(array $variables, string $hook, array $info): array {
    $suggestions = $this->buildSuggestions($variables, $hook, $info);
    $base_theme_hook = isset($info['base hook']) ? $info['base hook'] : $hook;
    $suggestions = [$base_theme_hook, ...$suggestions ];
    return array_values(array_unique(array_map([$this, 'normalize'], $suggestions)));
  }

  /**
   * Build suggestions based on the given hook_preprocess() variables.
   *
   * @see \Drupal\Core\Theme\ThemeManager::render
   *
   * @param array $variables
   * @param string $hook
   * @param array $info
   *
   * @return array
   */
  protected function buildSuggestions(array $variables, string $hook, array $info): array {
    // Set base hook for later use. For example if '#theme' => 'node__article'
    // is called, we run hook_theme_suggestions_node_alter() rather than
    // hook_theme_suggestions_node__article_alter(), and also pass in the base
    // hook as the last parameter to the suggestions alter hooks.
    if (isset($info['base hook'])) {
      $base_theme_hook = $info['base hook'];
    }
    else {
      $base_theme_hook = $hook;
    }

    // Invoke hook_theme_suggestions_HOOK().
    $suggestions = $this->moduleHandler->invokeAll('theme_suggestions_' . $base_theme_hook, [$variables]);
    // If the theme implementation was invoked with a direct theme suggestion
    // like '#theme' => 'node__article', add it to the suggestions array before
    // invoking suggestion alter hooks.
    if (isset($info['base hook'])) {
      $suggestions[] = $hook;
    }

    // Invoke hook_theme_suggestions_alter() and
    // hook_theme_suggestions_HOOK_alter().
    $hooks = [
      'theme_suggestions',
      'theme_suggestions_' . $base_theme_hook,
    ];
    $this->moduleHandler->alter($hooks, $suggestions, $variables, $base_theme_hook);
    $this->themeManager->alter($hooks, $suggestions, $variables, $base_theme_hook);

    return $suggestions;
  }

  /**
   * Normalize the given suggestion.
   *
   * @param string $suggestion
   *
   * @return string
   */
  protected function normalize(string $suggestion) {
    return trim(Html::getClass(str_replace('_', '-', $suggestion)), '-');
  }

}
