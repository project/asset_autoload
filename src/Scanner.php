<?php

namespace Drupal\asset_autoload;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Implements the scanner interface.
 *
 * @package Drupal\asset_autoload
 */
class Scanner implements ScannerInterface {

  /**
   * The extension helper.
   *
   * @var \Drupal\asset_autoload\ExtensionHelper
   */
  protected $extensionHelper;

  /**
   * The file system helper.
   *
   * @var \Drupal\asset_autoload\FileSystemHelper
   */
  protected $fileSystemHelper;

  /**
   * The discovery cache bin.
   * 
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a new discovery handler object.
   *
   * @param \Drupal\asset_autoload\ExtensionHelper $extensionHelper
   * @param \Drupal\asset_autoload\FileSystemHelper $fileSystemHelper
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   */
  public function __construct(
    ExtensionHelper $extensionHelper,
    FileSystemHelper $fileSystemHelper,
    CacheBackendInterface $cache,
  ) {
    $this->extensionHelper = $extensionHelper;
    $this->fileSystemHelper = $fileSystemHelper;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(string $extension): array {
    $data = $this->getData();

    return $data['libraries'][$extension] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensions(string $library): array {
    $data = $this->getData();

    return $data['extensions'][$library] ?? [];
  }

  /**
   * Returns the scanned data.
   */
  protected function getData(): array {
    static $data;

    if (is_array($data)) {
      return $data;
    }

    $cid = 'asset_autoload:data';

    if ($cache = $this->cache->get($cid)) {
      $data = $cache->data;
      return $data;
    }

    $data = $this->scan();
    $this->cache->set($cid, $data);

    return $data;
  }

  /**
   * Scans all extensions for assets.
   *
   * @return array
   */
  protected function scan(): array {
    $extension_infos = $this->extensionHelper->getAllInstalledInfo();
    $data = [];
    foreach ($extension_infos as $extension => $extension_info) {
      $this->scanExtension($data, $extension, $extension_info);
    }
    foreach ($data['libraries'] as $extension => $libraries) {
      $dependencies = $this->extensionHelper->getDependencies($extension);
      $dependencies = array_map(function ($dependency) {
        return $dependency->getName();
      }, $dependencies);
      foreach ($libraries as $library => $definition) {
        foreach ($dependencies as $dependency) {
          if (!isset($data['libraries'][$dependency][$library])) {
            continue;
          }
          $data['libraries'][$extension][$library]['dependencies'][] = "$dependency/$library";
        }
      }
    }
    return $data;
  }

  /**
   * Scans the specified extension for assets.
   *
   * @param array $data
   * @param string $extension
   * @param array $extension_info
   *
   * @return void
   */
  protected function scanExtension(array &$data, string $extension, array $extension_info): void {
    if (!array_key_exists('asset_autoload', $extension_info)) {
      return;
    }

    $extension_path = $this->extensionHelper->getPath($extension);
    $directories = [];
    if (is_bool($extension_info['asset_autoload']) && $extension_info['asset_autoload']) {
      $directories[] = '';
    }
    else if (is_string($extension_info['asset_autoload'])) {
      $directories[] = $extension_info['asset_autoload'];
    }
    else if (is_array($extension_info['asset_autoload'])) {
      $directories = $extension_info['asset_autoload'];
    }

    foreach ($directories as $directory) {
      $scan_path = "$extension_path/$directory";
      $files = $this->fileSystemHelper->scanDirectory($scan_path, '/\.(css|js)$/');
      foreach ($files as $file) {
        $library = $file->name;
        $path = substr($file->uri, strlen($extension_path) + 1);
        $file_extension = pathinfo($file->filename, PATHINFO_EXTENSION);

        if ($file_extension === 'css') {
          $data['libraries'][$extension][$library]['css']['theme'][$path] = [];
        }
        else if ($file_extension === 'js') {
          $data['libraries'][$extension][$library]['js'][$path] = [];
        }

        $data['extensions'][$library][$extension] = $extension_info;
      }
    }
  }

}
