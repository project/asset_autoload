INTRODUCTION
------------

Asset Autoload takes the hard work out of maintaining a large number of asset
libraries by defining a Drupal-like naming convention based on theme suggestions
for autoloading CSS, JS and preprocess files.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/asset_autoload

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/asset_autoload


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

However, we highly suggest preprocessing your CSS and JS using a frontend build
tool such as Parcel or Webpack.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Asset Autoload works on a per module/theme basis. If you want your theme to use
Asset Autoload, you must opt in to the functionality by adding the following to
your *.info.yml file:

```
asset_autoload: true
```


MAINTAINERS
-----------

Current maintainers:
 * Parkle Lee (phparkle) - https://www.drupal.org/u/phparkle
